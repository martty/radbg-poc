#include <stdio.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/uio.h>
#include <sys/syscall.h>      /* Definition of SYS_* constants */
#include <unistd.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdint.h>
#include <assert.h>
#include <fcntl.h>
#include <string.h>
extern "C" {
#include "umr.h"
}
#include <libdrm/amdgpu.h>
#include <xf86drm.h>
//#include "drm-uapi/amdgpu_drm.h"
extern "C" {
#include "winsys/amdgpu/radv_amdgpu_winsys_public.h"
}
#include "radv_radeon_winsys.h"
#include <time.h>
void timestamp()
{
    time_t ltime; /* calendar time */
    ltime=time(NULL); /* get current cal time */
    fprintf(stderr, "%s",asctime( localtime(&ltime) ) );
}

void procmsg(const char *format, ...)
{
    va_list ap;
    fprintf(stdout, "[%d] ", getpid());
    va_start(ap, format);
    vfprintf(stdout, format, ap);
    va_end(ap);
}

struct umr_asic *asic;

int stop_or_resume_waves(bool stop){
    struct umr_reg *reg;
    uint32_t value;
    uint64_t addr;
        struct {
            uint32_t se, sh, instance, use_grbm;
        } grbm;


    reg = umr_find_reg_data(asic, "mmSQ_CMD");
	if (!reg) {
		asic->err_msg("[BUG]: Cannot find SQ_CMD register in umr_sq_cmd_halt_waves()\n");
		return -1;
	}
	/* copy grbm options to restore later */
	grbm.use_grbm = asic->options.use_bank;
	grbm.se       = asic->options.bank.grbm.se;
	grbm.sh       = asic->options.bank.grbm.sh;
	grbm.instance = asic->options.bank.grbm.instance;
    
    value = umr_bitslice_compose_value(asic, reg, "CMD", 1); // SETHALT
	value |= umr_bitslice_compose_value(asic, reg, "DATA", (uint32_t)stop);

  	value |= umr_bitslice_compose_value(asic, reg, "MODE", 1); // BROADCAST

	/* set GRBM banking options */
	asic->options.use_bank           = 1;
	asic->options.bank.grbm.se       = 0xFFFFFFFF;
	asic->options.bank.grbm.sh       = 0xFFFFFFFF;
	asic->options.bank.grbm.instance = 0xFFFFFFFF;

	// compose address
	addr = reg->addr * 4;
	asic->reg_funcs.write_reg(asic, addr, value, reg->type);

	/* restore whatever the user had picked */
	asic->options.use_bank           = grbm.use_grbm;
	asic->options.bank.grbm.se       = grbm.se;
	asic->options.bank.grbm.sh       = grbm.sh;
	asic->options.bank.grbm.instance = grbm.instance;

    return 0;
}

void halt_gfx_ring(){
    stop_or_resume_waves(true);
    usleep(200);
    char* ring_name = "gfx_0.0.0";
    for(int i = 0; i < 10 && !umr_ring_is_halted(asic, ring_name); i++){
        stop_or_resume_waves(true);
        usleep(1000);
    }
    assert(umr_ring_is_halted(asic, ring_name));
}

void resume_gfx_ring(){
    stop_or_resume_waves(false);
}

#define SQ_SHADER_TBA_HI__TRAP_EN__SHIFT                                                                      0x1f

int enable_debugging(uint64_t tba_va, uint64_t tma_va){
    umr_reg* reg_tba_lo = umr_find_reg_data(asic, "mmSQ_SHADER_TBA_LO");
    if(!reg_tba_lo){
        asic->err_msg("[BUG]: Cannot find TBA_LO register in umr_sq_cmd_halt_waves()\n");
        return -1;
    }

    umr_reg* reg_tba_hi = umr_find_reg_data(asic, "mmSQ_SHADER_TBA_HI");
    if(!reg_tba_hi){
        asic->err_msg("[BUG]: Cannot find TBA_HI register in umr_sq_cmd_halt_waves()\n");
        return -1;
    }

    umr_reg* reg_tma_lo = umr_find_reg_data(asic, "mmSQ_SHADER_TMA_LO");
    if(!reg_tma_lo){
        asic->err_msg("[BUG]: Cannot find TMA_LO register in umr_sq_cmd_halt_waves()\n");
        return -1;
    }

    umr_reg* reg_tma_hi = umr_find_reg_data(asic, "mmSQ_SHADER_TMA_HI");
    if(!reg_tma_hi){
        asic->err_msg("[BUG]: Cannot find TMA_HI register in umr_sq_cmd_halt_waves()\n");
        return -1;
    }
  
    asic->options.use_bank = 2;
    asic->options.bank.srbm.me = 0;
    asic->options.bank.srbm.pipe = 0;
    for(int i = 1; i < 15; i++){
        asic->options.bank.srbm.vmid = i;
        
        asic->reg_funcs.write_reg(asic, reg_tba_lo->addr * 4, tba_va >> 8, reg_tba_lo->type);
        asic->reg_funcs.write_reg(asic, reg_tba_hi->addr * 4, (tba_va >> 40) | (1 << SQ_SHADER_TBA_HI__TRAP_EN__SHIFT), reg_tba_hi->type);
        asic->reg_funcs.write_reg(asic, reg_tma_lo->addr * 4, tma_va >> 8, reg_tma_lo->type);
        asic->reg_funcs.write_reg(asic, reg_tma_hi->addr * 4, (tma_va >> 40), reg_tma_hi->type);
    }

    return 0;
}

int disable_debugging(){
    umr_reg *reg = umr_find_reg_data(asic, "mmSQ_SHADER_TBA_HI");
    if(!reg){
        asic->err_msg("[BUG]: Cannot find TBA_HI register in umr_sq_cmd_halt_waves()\n");
        return -1;
    }

    asic->options.use_bank = 2;
    asic->options.bank.srbm.me = 0;
    asic->options.bank.srbm.pipe = 0;

    for(int i = 1; i < 15; i++){
        asic->options.bank.srbm.vmid = i;
        
        unsigned v = asic->reg_funcs.read_reg(asic, reg->addr * 4, reg->type);
        asic->reg_funcs.write_reg(asic, reg->addr * 4, v & ~(1 << SQ_SHADER_TBA_HI__TRAP_EN__SHIFT), reg->type);
    }

    return 0;
}

struct umr_options options = {};

static int std_printf(const char *fmt, ...)
{
	va_list ap;
	int r;

	va_start(ap, fmt);
	r = vfprintf(stdout, fmt, ap);
	fflush(stdout);
	va_end(ap);
	return r;
}

static int err_printf(const char *fmt, ...)
{
	va_list ap;
	int r;

	va_start(ap, fmt);
	r = vfprintf(stderr, fmt, ap);
	fflush(stderr);
	va_end(ap);
	return r;
}

void init_umr(){
	asic = umr_discover_asic(&options, std_printf);
	if (!asic) {
		printf("ASIC not found (instance=%d, did=%08lx)\n", options.instance, (unsigned long)options.forcedid);
		exit(EXIT_FAILURE);
	}
	umr_scan_config(asic, 1);

	// assign linux callbacks
	asic->err_msg = err_printf;
	asic->std_msg = std_printf;
	asic->mem_funcs.vm_message = std_printf;
	asic->mem_funcs.gpu_bus_to_cpu_address = umr_vm_dma_to_phys;
	asic->mem_funcs.access_sram = umr_access_sram;

    asic->options.use_pci = 0;
    asic->options.no_kernel = 0;
	if (asic->options.use_pci == 0)
		asic->mem_funcs.access_linear_vram = umr_access_linear_vram;
	else
		asic->mem_funcs.access_linear_vram = umr_access_vram_via_mmio;

	asic->reg_funcs.read_reg = umr_read_reg;
	asic->reg_funcs.write_reg = umr_write_reg;

	asic->wave_funcs.get_wave_sq_info = umr_get_wave_sq_info;
	asic->wave_funcs.get_wave_status = umr_get_wave_status;
	asic->shader_disasm_funcs.disasm = umr_shader_disasm;

	asic->gpr_read_funcs.read_sgprs = umr_read_sgprs;
	asic->gpr_read_funcs.read_vgprs = umr_read_vgprs;

	// default shader options
	if (asic->family <= FAMILY_VI) { // on gfx9+ hs/gs are opaque
		asic->options.shader_enable.enable_gs_shader = 1;
		asic->options.shader_enable.enable_hs_shader = 1;
	}
	asic->options.shader_enable.enable_vs_shader   = 1;
	asic->options.shader_enable.enable_ps_shader   = 1;
	asic->options.shader_enable.enable_es_shader   = 1;
	asic->options.shader_enable.enable_ls_shader   = 1;
	asic->options.shader_enable.enable_comp_shader = 1;

    asic->options.skip_gprs = 0;

	if (asic->family > FAMILY_VI)
		asic->options.shader_enable.enable_es_ls_swap = 1;  // on >FAMILY_VI we swap LS/ES for HS/GS

	umr_create_mmio_accel(asic);

    if (asic->fd.gfxoff >= 0) {
		uint32_t value = 0;
		write(asic->fd.gfxoff, &value, sizeof(value));
        fprintf(stderr, "gfxoff disabled\n");
    }
}

radeon_winsys* boot_winsys(){
    // boot winsys
    drmDevicePtr devices[8];
    int max_devices = drmGetDevices2(0, devices, ARRAY_SIZE(devices));

    if (max_devices < 1)
    {
        fprintf(stderr, "Could not enum drm devices.\n");
        return nullptr;
    }

    int fd = -1;

    auto drm_device = devices[0];

    if (drm_device)
    {
        const char *path = drm_device->nodes[DRM_NODE_RENDER];
        drmVersionPtr version;

        fd = open(path, O_RDWR | O_CLOEXEC);
        if (fd < 0)
        {
            fprintf(stderr, "Could not open device: %m\n", path);
            return nullptr;
        }

        version = drmGetVersion(fd);
        if (!version)
        {
            close(fd);

            fprintf(stderr, "Could not get the kernel driver version for device: %m\n", path);
            return nullptr;
        }

        if (strcmp(version->name, "amdgpu"))
        {
            drmFreeVersion(version);
            close(fd);

            fprintf(stderr, "Device is not using the AMDGPU kernel driver: %m\n", path);
            return nullptr;
        }
        drmFreeVersion(version);

        fprintf(stderr, "radv: info: Found compatible device '%s'.\n", path);
    }
    auto ws = radv_amdgpu_winsys_create(fd, 0, 0, false);
    drmFreeDevices(devices, max_devices);
    return ws;
}

radeon_winsys_bo* out_bo;
volatile uint32_t* map_tma(radeon_winsys* ws, int child_pidfd) {
    FILE *f = fopen("/home/deck/tma_fd", "rb");
    assert(f);
    int tgt_fd = 0;
    fread(&tgt_fd, sizeof(int), 1, f);
    fclose(f);

    auto res = syscall(SYS_pidfd_getfd, child_pidfd, tgt_fd, 0);
    if(res < 0)
        perror("pidfd_getfd");
    tgt_fd = res;
    uint64_t alloc_size;
    auto result = ws->buffer_from_fd(ws, tgt_fd, RADV_BO_PRIORITY_SCRATCH, &out_bo, &alloc_size);
    if (result != 0){
        fprintf(stderr, "buf from fd fail(%d) %p\n", tgt_fd, out_bo);
        perror("fd");
    }

    volatile uint32_t* base_ptr = (volatile uint32_t*) ws->buffer_map(out_bo);
    return base_ptr;
}
#define MANY_TO_INSTANCE(wgp, simd) (((simd) & 3) | ((wgp) << 2))

#define SGPR_OFFSET 0x200
#define VGPR_OFFSET 0x400

void upload_vgpr(unsigned se, unsigned sa, unsigned wgp, unsigned simd, unsigned wave, unsigned vgpr, unsigned thread, unsigned value){
    struct umr_reg *ind_index, *ind_data;
	uint32_t data;

    // GPR R/W uses GRBM banking
    asic->options.use_bank           = 1;
	asic->options.bank.grbm.se       = se;
	asic->options.bank.grbm.sh       = sa;
	asic->options.bank.grbm.instance = (wgp << 2) | simd;

	ind_index = umr_find_reg_data(asic, "mmSQ_IND_INDEX");
	ind_data  = umr_find_reg_data(asic, "mmSQ_IND_DATA");

    unsigned granularity = asic->parameters.vgpr_granularity;

	if (ind_index && ind_data) {
		data = umr_bitslice_compose_value(asic, ind_index, "WAVE_ID", wave);
		data |= umr_bitslice_compose_value(asic, ind_index, "INDEX", VGPR_OFFSET + vgpr);
		data |= umr_bitslice_compose_value(asic, ind_index, "WORKITEM_ID", thread);
		umr_write_reg(asic, ind_index->addr * 4, data, REG_MMIO);
		umr_write_reg(asic, ind_data->addr * 4, value, REG_MMIO);
	} else {
		asic->err_msg("[BUG]: The required SQ_IND_{INDEX,DATA} registers are not found on the asic <%s>\n", asic->asicname);
		return;
	}

    asic->options.use_bank           = 1;
	asic->options.bank.grbm.se       = 0xFFFFFFFF;
	asic->options.bank.grbm.sh       = 0xFFFFFFFF;
	asic->options.bank.grbm.instance = 0xFFFFFFFF;
}


/**
 * Scan the given wave slot. Return true and fill in \p pwd if a wave is present.
 * Otherwise, return false.
 *
 * \param cu the CU on <=gfx9, the WGP on >=gfx10
 */
static int umr_scan_wave_slot(struct umr_asic *asic, uint32_t se, uint32_t sh, uint32_t cu,
			       uint32_t simd, uint32_t wave, struct umr_wave_data *pwd)
{
	unsigned thread, num_threads;
	int r;

	if (asic->family <= FAMILY_AI)
		r = asic->wave_funcs.get_wave_status(asic, se, sh, cu, simd, wave, &pwd->ws);
	else
		r = asic->wave_funcs.get_wave_status(asic, se, sh, MANY_TO_INSTANCE(cu, simd), 0, wave, &pwd->ws);

	if (r)
		return -1;

	if (!pwd->ws.wave_status.valid &&
	    (!pwd->ws.wave_status.halt || pwd->ws.wave_status.value == 0xbebebeef))
		return 0;

	pwd->se = se;
	pwd->sh = sh;
	pwd->cu = cu;
	pwd->simd = simd;
	pwd->wave = wave;

	if (!asic->options.skip_gprs) {
		asic->gpr_read_funcs.read_sgprs(asic, &pwd->ws, &pwd->sgprs[0]);

		if (asic->family <= FAMILY_AI)
			num_threads = 64;
		else
			num_threads = pwd->ws.ib_sts2.wave64 ? 64 : 32;

		pwd->have_vgprs = 1;
		pwd->num_threads = num_threads;
		for (thread = 0; thread < num_threads; ++thread) {
			if (asic->gpr_read_funcs.read_vgprs(asic, &pwd->ws, thread,
					   &pwd->vgprs[256 * thread]) < 0) {
				pwd->have_vgprs = 0;
				break;
			}
		}
	} else {
		pwd->have_vgprs = 0;
	}

	return 1;
}


#define NUM_OPCODE_WORDS 1
#define PP(x, y) if (wd->ws.x.y != 0xFFFFFFFF) { if (col++ == 4) { col = 1; printf("\n\t"); } printf("%s%20s%s: %s%8u%s | ", GREEN, #y, RST, BLUE, (unsigned)wd->ws.x.y, RST); }
#define PX(x, y) if (wd->ws.x.y != 0xFFFFFFFF) { if (col++ == 4) { col = 1; printf("\n\t"); } printf("%s%20s%s: %s%08lx%s | ", GREEN, #y, RST, BLUE, (unsigned long)wd->ws.x.y, RST); }

#define P(x) if (col++ == 4) { col = 1; printf("\n\t"); } printf("%s%20s%s: %s%8u%s | ", GREEN, #x, RST, BLUE, (unsigned)wd->ws.x, RST);
#define X(x) if (col++ == 4) { col = 1; printf("\n\t"); } printf("%s%20s%s: %s%08lx%s | ", GREEN, #x, RST, BLUE, (unsigned long)wd->ws.x, RST);

#define H(x) if (col) { printf("\n"); }; col = 0; printf("\n\n%s:\n\t", x);
#define Hv(x, y) if (col) { printf("\n"); }; col = 0; printf("\n\n%s[%08lx]:\n\t", x, (unsigned long)y);

uint vgpr_rotate_counter = 0;

void dump_wave(unsigned se, unsigned sa, unsigned wgp, unsigned simd, unsigned wave, bool wp){
    umr_packet_stream* stream = nullptr;

    halt_gfx_ring();
    //stream = umr_packet_decode_ring(asic, NULL, ring_name, 0, -1, -1, UMR_RING_GUESS);

    umr_wave_data wdt = {};
    umr_wave_data* wd = &wdt;
    bool success = false;
    int tries = 0;
    uint64_t fault_pc;
    int result = umr_scan_wave_slot(asic, se, sa, wgp, simd, wave, wd);
    unsigned ttmp[16];
    memcpy(ttmp, &wd->sgprs[0x6C], sizeof(unsigned) * 16);
    unsigned fault_lo = ttmp[0];
    unsigned fault_hi = ttmp[1] & 0xffff;
    fault_pc = (((uint64_t)fault_hi << 32) | fault_lo);
    uint64_t pc = (((uint64_t)wd->ws.pc_hi << 32) | wd->ws.pc_lo);
    success = result == 1 && fault_pc != 0xbeefbebebeef && pc != 0xbeefbebebeef && fault_lo > 0 && fault_hi > 0 && wd->ws.pc_hi > 0 && wd->ws.pc_lo > 0;
    if (!success){
        fprintf(stderr, "failed to scan\n");
        resume_gfx_ring();
        return;
    }

    if(fault_pc == 0x800000004f40){ // before packing v2 and v3, we rotate v2
        for (int thread = 0; thread < wd->num_threads; ++thread) {
            auto prev_thread = (thread + wd->num_threads - vgpr_rotate_counter) % wd->num_threads;

            float fv = *(float*)&wd->vgprs[prev_thread * 256 + 0];
            upload_vgpr(se, sa, wgp, simd, wave, 0, thread, *(unsigned*)&fv);
            fv = *(float*)&wd->vgprs[prev_thread * 256 + 2];
            upload_vgpr(se, sa, wgp, simd, wave, 2, thread, *(unsigned*)&fv);
            fv = *(float*)&wd->vgprs[prev_thread * 256 + 3];
            upload_vgpr(se, sa, wgp, simd, wave, 3, thread, *(unsigned*)&fv);
        };

        vgpr_rotate_counter = (vgpr_rotate_counter + 1) % 64;
    }

    fprintf(stderr, "\nPC=0x%" PRIx64 " FAULT_PC=0x%" PRIx64 " m0=%u\n", pc, fault_pc, wd->ws.m0);

    uint32_t shader_size = NUM_OPCODE_WORDS*4*2;
    uint64_t pgm_addr = fault_pc;
    uint64_t shader_addr;
    struct umr_shaders_pgm *shader = NULL;
    if (stream)
        shader = umr_packet_find_shader(stream, wd->ws.hw_id2.vm_id, pgm_addr);
    if (shader) {
        // start decoding a bit before PC if possible
        if (shader->addr + ((NUM_OPCODE_WORDS*4)/2) < pgm_addr)
            pgm_addr -= (NUM_OPCODE_WORDS*4)/2;
        else
            pgm_addr = shader->addr;
        shader_addr = shader->addr;
        free(shader);
    } else {
        //pgm_addr -= (NUM_OPCODE_WORDS*4)/2;
        shader_addr = pgm_addr;
        printf("\n");
    }
    if(wp){
        umr_vm_disasm(asic, -1, wd->ws.hw_id2.vm_id, shader_addr + 4, fault_pc + 4, shader_size, 0, NULL);
    } else {
        umr_vm_disasm(asic, -1, wd->ws.hw_id2.vm_id, shader_addr, fault_pc, shader_size, 0, NULL);
    }

	if (stream)
		umr_packet_free(stream);

    resume_gfx_ring();
}

void run_target(const char *programname)
{
    procmsg("target started. will run '%s'\n", programname);

    /* Allow tracing of this process */
    if (ptrace(PTRACE_TRACEME, 0, 0, 0) < 0)
    {
        perror("ptrace");
        return;
    }

    /* Replace this process's image with the given program */
    execl(programname, programname, 0);
}

volatile sig_atomic_t do_loop = true;

void inthand(int signum) {
    do_loop = false;
}

int run_debugger_attach(pid_t child_pid)
{
    signal(SIGINT, inthand);
    int wait_status;
    unsigned icounter = 0;
    procmsg("debugger(attach) started\n");
    int child_pidfd = syscall(SYS_pidfd_open, child_pid, 0);
    if(child_pidfd < 0)
        perror("pidfd_open");

    procmsg("mapping tma\n");
    
    auto ws = boot_winsys();
    auto base_ptr = map_tma(ws, child_pidfd);
    assert(base_ptr);
    procmsg("enabling debugging");
    stop_or_resume_waves(false);
    FILE* f = fopen("/home/deck/tba", "rb");
    assert(f);
    uint64_t tba_va; uint64_t tma_va;
    fread(&tba_va, sizeof(uint64_t), 1, f);
    fclose(f);
    f = fopen("/home/deck/tma", "rb");
    assert(f);
    fread(&tma_va, sizeof(uint64_t), 1, f);
    fclose(f);
    fprintf(stderr, "TBA:%p TMA:%p\n", tba_va, tma_va);
    enable_debugging(tba_va, tma_va);

    memset((void*)(base_ptr + 4), 0, 4096-16);
    stop_or_resume_waves(false);
    
    fprintf(stderr, "enabling wavepoint\n");
    base_ptr[0+4] = 1;

    while(do_loop){
        // wait for wavepoint hit
        while(base_ptr[3+4] == 0 && do_loop) {
            usleep(10);
        }

        if(!do_loop){
            break;
        }

        fprintf(stderr, "wavepoint hit, stepping-and-dumping\n");
        auto value = base_ptr[6+4];
        auto reg = umr_find_reg_data(asic, "ixSQ_WAVE_HW_ID1");
        auto wave_id = umr_bitslice_reg(asic, reg, "WAVE_ID", value);
        auto simd_id = umr_bitslice_reg(asic, reg, "SIMD_ID", value);
        auto wgp_id  = umr_bitslice_reg(asic, reg, "WGP_ID", value);
        auto sa_id   = umr_bitslice_reg(asic, reg, "SA_ID", value);
        auto se_id   = umr_bitslice_reg(asic, reg, "SE_ID", value);

        fprintf(stderr, "se %u sa %u wgp %u simd %u wave %u\n", se_id, sa_id, wgp_id, simd_id, wave_id);
        dump_wave(se_id, sa_id, wgp_id, simd_id, wave_id, true);
        base_ptr[1+4] = 1;

        // single step mode - we wait until a new step is complete, then dump / modify wave state

        unsigned cntr = 0;
        for(unsigned j = 0; j < 0xc; j++){
            int nval = base_ptr[2+4];
            int timeout = 0;
            const int timeout_cnt = 100;
            for(; timeout < timeout_cnt && nval == cntr && do_loop; timeout++, nval = base_ptr[2+4]) {
                usleep(10);
            }
            if(timeout != timeout_cnt){
                cntr = nval;
                dump_wave(se_id, sa_id, wgp_id, simd_id, wave_id, false);
                fprintf(stderr, "-----------------------------\n");
            } else {
                fprintf(stderr,"timeout, unspin and break\n");
                for (unsigned i = 0; i < 120; i++) {
                    fprintf(stderr, "tma_ptr[%d]=0x%x\n", i,base_ptr[i]);
                }
                break;
            }

            if(j != 0xe)
                base_ptr[1+4] = 1; // disable spin
        }

        if(do_loop){
            base_ptr[0+4] = 1; // reenable trap
            base_ptr[3+4] = 0;
            base_ptr[2+4] = 0; // clear cntrs
            base_ptr[1+4] = 1; // disable spin
        }
    }

    for(unsigned j = 0; j < 100; j++){
        base_ptr[1+4] = 1;
        usleep(1000);
    }

    disable_debugging();
    ws->destroy(ws);

    return 0;
}

int main(int argc, char **argv)
{
    pid_t child_pid;

    if (argc < 2)
    {
        fprintf(stderr, "Expected a program name as argument\n");
        return -1;
    }

    init_umr();
    fprintf(stderr, "umr ready.\n");

    if(argc == 2) {
        child_pid = fork();
    } else if (argc == 3) {
        assert(strcmp(argv[1], "-p") == 0);
        child_pid = atoi(argv[2]);
        return run_debugger_attach(child_pid);
    } else {
        fprintf(stderr, "Expected one or two arguments\n");
        return -1;
    }
    if (child_pid == 0)
        run_target(argv[1]);
    else if (child_pid > 0)
        return -1;
    else
    {
        perror("fork");
        return -1;
    }

    return 0;
}